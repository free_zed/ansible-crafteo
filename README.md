`ansible.training.crafteo.io`
-----------------------------

Ansible training pages for [Crafteo - crafteo.io](https://crafteo.io)

Forked & squashed from : [`PierreBeucher/workshop-ansible-playbook`](https://github.com/PierreBeucher/workshop-ansible-playbook)

* My code : [`my-code`](https://gitlab.com/free_zed/ansible-crafteo/-/commits/my-code)
* Correction : [`correction`](https://gitlab.com/free_zed/ansible-crafteo/-/commits/correction)
* Related blog post : _«[
e-Workshop : introduction à Ansible
](http://pro.zind.fr/articles/2020/07/e-workshop-introduction-a-ansible/)»_
